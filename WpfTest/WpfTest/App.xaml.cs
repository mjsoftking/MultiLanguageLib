﻿using MultiLanguageLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace WpfTest
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            //初始化多语言
            Dictionary<string, string> map = new Dictionary<string, string>();
            map.Add("cn", @"cn.xaml");
            map.Add("en", @"en.xaml");
            MultiLanguage.Init(map);
            
            //TODO 在调用此函数前必须在 App.xaml下配置相关代码，否则会抛出数组越界异常
            //TODO 在此处根据记录的最后设置语言状态码，切换语言
            MultiLanguage.SwitchLanguage("cn");

        }

    }
}
