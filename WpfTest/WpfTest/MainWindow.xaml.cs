﻿using MultiLanguageLib;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfTest
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Label la = new Label();
            la.Width = 200;
            la.Height = 100;
            la.HorizontalContentAlignment = HorizontalAlignment.Center;
            la.VerticalContentAlignment = VerticalAlignment.Center;
            la.Content = "10";
            la.MouseDown += La_MouseDown;
            this.wrapPanel.Children.Add(la);
 
        }

        private void La_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is Label)
            {
                this.wrapPanel.Children.Remove((Label)sender);
            }


            if (MultiLanguage.CurrentLanguageKey().Equals("en"))
            {
                MultiLanguage.SwitchLanguage("cn");
            }
            else if (MultiLanguage.CurrentLanguageKey().Equals("cn"))
            {
                MultiLanguage.SwitchLanguage("en");
            }


            MessageBox.Show(MultiLanguage.TryFindResource("two"));
        }
    }
}
