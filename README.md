# MultiLanguageLib

[![License](https://img.shields.io/badge/License%20-Apache%202-337ab7.svg)](https://www.apache.org/licenses/LICENSE-2.0)

#### 介绍

 **C# WPF 的多国语言处理，主要利用资源字典实现** 

#### 类库

MultiLanguageLib 为类库资源，生成dll引用至主项目或者直接复制内部的单个类文件到主项目即可

#### 使用

1. 主项目引入dll文件或者直接复制内部的单个类文件；

2. 主项目添加 **资源字典** ，起名，放在自己需要的文件夹下；

3. 主项目 **app.xaml** 添加

   
```c#
 <Application.Resources>
        <ResourceDictionary>
            <ResourceDictionary.MergedDictionaries>
                <ResourceDictionary Source="cn.xaml（此处为默认使用的资源字典文件，如果在文件夹下面，则需要清楚填写对应位置）"/>

                <!--若有其他styly资源字典，需要保证第一个位置为文本类资源字典，本例默认第一个为程序语言类资源字典-->

            </ResourceDictionary.MergedDictionaries>
        </ResourceDictionary>
    </Application.Resources>
```
4. 初始化字典以及程序重启后仍能使用最后设置的语言  **App.xaml.cs**  文件下
```c#
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            //初始化多语言
            Dictionary<string, string> map = new Dictionary<string, string>();
            map.Add("cn", @"cn.xaml"); ///@"cn.xaml"为资源字典的相对路径，若出现找不到的问题，请使用绝对路径，使用内置资源的绝对路径表达方式
            map.Add("en", @"en.xaml");
            MultiLanguage.Init(map);
            
            //TODO 在调用此函数前必须在 App.xaml下配置相关代码，否则会抛出数组越界异常
            //TODO 在此处根据记录的最后设置语言状态码，切换语言
            MultiLanguage.SwitchLanguage("cn");

        }

    }
```


1. 窗体控件引用字典资源
```c#
    Title="{DynamicResource buttonNewTaskWindow（此字段为资源字典内的资源key）}"
```

2. 代码内切换资源字典，如需保证程序重新启动后仍然有效，则在此处保存你的语言字典状态码

```c#
    private void La_MouseDown(object sender, MouseButtonEventArgs e)
    {
        //切换语言
        if (MultiLanguage.CurrentLanguageKey().Equals("en"))
        {
            MultiLanguage.SwitchLanguage("cn");
        }
        else if (MultiLanguage.CurrentLanguageKey().Equals("cn"))
        {
            MultiLanguage.SwitchLanguage("en");
        }
    
        //使用当前确定的语言，读取资源字典内容，参数为资源字典key
        MessageBox.Show(MultiLanguage.TryFindResource("two"));
    }
```


License
-------

    Copyright 2021 mjsoftking

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

