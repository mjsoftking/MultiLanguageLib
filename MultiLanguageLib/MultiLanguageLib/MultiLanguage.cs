using System;
using System.Windows;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MultiLanguageLib
{
    /// <summary>
    /// 多语言操作类
    /// </summary>
    public class MultiLanguage
    {
        private static Dictionary<string, string> map;

        private MultiLanguage() { }

        /// <summary>
        /// 使用 key - value形式初始化多语言控制库，使用前必须执行
        /// </summary>
        /// <param name="ht">key:string; value:string</param>
        public static void Init(Dictionary<string, string> ht)
        {
            map = ht;
        }

        /// <summary>
        /// 切换语言
        /// </summary>
        /// <param name="key">Hashtable下存在的key，根据此key获取对应的资源文件</param>
        public static void SwitchLanguage(string key)
        {
            if (null == map)
            {
                throw new NullReferenceException("初始化多语言Hashtable后使用");
            }
            var value = map[key];
            if (null == value)
            {
                throw new NullReferenceException(string.Format("\"{0}\"无效，Hashtable未找到", key));
            }
            ResourceDictionary resourceDictionary = new ResourceDictionary();
            resourceDictionary.Source = new Uri(@value, UriKind.Relative);
            Application.Current.Resources.MergedDictionaries[0] = resourceDictionary;
        }

        /// <summary>
        /// 获取当前使用的语言在Hashtable的key的表现形式
        /// </summary>
        public static string CurrentLanguageKey()
        {
            if (null == map)
            {
                throw new NullReferenceException("初始化多语言Hashtable后使用");
            }
            string value = Application.Current.Resources.MergedDictionaries[0].Source.OriginalString;

            try
            {
                return (from obj in map.Keys
                        where map[obj].Equals(value)
                        select obj).FirstOrDefault();
            }
            catch
            {
                throw new NullReferenceException(string.Format("Hashtable未匹配到当前语言"));
            }
        }

        /// <summary>
        /// 根据资源key获取对应的资源值
        /// </summary>
        /// <param name="resourseKey">资源key，非Hashtable的key</param>
        /// <returns>获取到当前使用的语言对应key下的值，取不到返回空字符串</returns>
        public static string TryFindResource(string resourseKey)
        {
            return Application.Current.Resources[resourseKey] as string;
        }

    }
}
